/*
 * msp.c
 *
 *  Created on: Dec 30, 2020
 *      Author: Developer
 */
#include "main.h"
#include "stm32f4xx_hal_gpio.h"
 void HAL_MspInit(void)
{
  /* NOTE : This function should not be modified, when the callback is needed,
            the HAL_MspInit could be implemented in the user file
   */
	 //1.priority grouping
	 // we use default one it is under hal driver layer under hal_cortex.c
	 HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);
	 //2.system exceptions
	 SCB->SHCSR |=SCB_SHCSR_USGFAULTENA_Msk ; //enable usage
	 SCB->SHCSR |=SCB_SHCSR_BUSFAULTENA_Msk ; //mem
	 SCB->SHCSR |=SCB_SHCSR_MEMFAULTENA_Msk ; //bus
	 //3.priority for system exceptions

	 HAL_NVIC_SetPriority(MemoryManagement_IRQn, 0, 0);
	 HAL_NVIC_SetPriority(BusFault_IRQn, 0, 0);
	 HAL_NVIC_SetPriority(UsageFault_IRQn, 0, 0);
}


  void HAL_UART_MspInit(UART_HandleTypeDef *huart)
 {
	  // low level initialization

	  //enable clock for usart2
	  __HAL_RCC_USART2_CLK_ENABLE();

	  //pin muxing

      __HAL_RCC_GPIOA_CLK_ENABLE();
	  GPIO_InitTypeDef gpioa_uart;
	  gpioa_uart.Pin =GPIO_PIN_2;
	  gpioa_uart.Mode =GPIO_MODE_AF_PP;
	  gpioa_uart.Speed =GPIO_SPEED_FREQ_HIGH;
	  gpioa_uart.Pull=GPIO_PULLUP;
	  gpioa_uart.Alternate=GPIO_AF7_USART2;

	  HAL_GPIO_Init(GPIOA, &gpioa_uart);

	  gpioa_uart.Pin =GPIO_PIN_3;
	  HAL_GPIO_Init(GPIOA, &gpioa_uart);

	  //enable irq ,priority NVIC
	  HAL_NVIC_EnableIRQ(USART2_IRQn);
	  HAL_NVIC_SetPriority(USART2_IRQn, 15, 0);
 }

