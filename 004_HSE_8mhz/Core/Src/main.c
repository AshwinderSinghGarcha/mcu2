

/*
 * main.c
 *
 *  Created on: Dec 30, 2020
 *      Author: Developer
 */
#include<string.h>
#include<stdio.h>
#include "stm32f4xx_hal.h"
#include "main.h"

#define TRUE  1
#define FALSE 0



void UART2_Init(void);
void Error_handler();




UART_HandleTypeDef huart2;




int main (void)
{
	char msg[100];
	RCC_OscInitTypeDef osc_init;
	RCC_ClkInitTypeDef clk_init;

    HAL_Init();

	UART2_Init();
//-------------------------------------------------------------
	osc_init.OscillatorType = RCC_OSCILLATORTYPE_HSE;
	osc_init.HSEState       = RCC_HSE_ON ;

	if (HAL_RCC_OscConfig(&osc_init) != HAL_OK)
		{
		Error_handler();
		}

	clk_init.SYSCLKSource   = RCC_SYSCLKSOURCE_HSE ;
	clk_init.ClockType      = RCC_CLOCKTYPE_SYSCLK |RCC_CLOCKTYPE_HCLK |\
			                  RCC_CLOCKTYPE_PCLK1 |RCC_CLOCKTYPE_PCLK2;

	clk_init.AHBCLKDivider  = RCC_SYSCLK_DIV2;
    clk_init.APB1CLKDivider = RCC_HCLK_DIV2 ;
    clk_init.APB2CLKDivider = RCC_HCLK_DIV2 ;
	//program carefully flash latency becz we use less than 30mhz
    if (HAL_RCC_ClockConfig(&clk_init, FLASH_ACR_LATENCY_0WS)!= HAL_OK)
            {
    		Error_handler();
    		}

    __HAL_RCC_HSI_DISABLE();       //turn off HSI to save current


    /*--------------------------------configure systick
     *
     *
     */
    HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);              //4mhz/1000   hclk /1ms
    HAL_SYSTICK_CLKSourceConfig(HAL_RCC_GetHCLKFreq());
    UART2_Init();
//-----------------------------------------------------------------------------
    memset(msg,0,sizeof(msg));
    sprintf(msg,"SYSCLK : %ld\r\n",HAL_RCC_GetSysClockFreq());
    HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY);

    memset(msg,0,sizeof(msg));
    sprintf(msg,"HCLK   : %ld\r\n",HAL_RCC_GetHCLKFreq());
    HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY);

    memset(msg,0,sizeof(msg));
    sprintf(msg,"PCLK1  : %ld\r\n",HAL_RCC_GetPCLK1Freq());
    HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY);

    memset(msg,0,sizeof(msg));
    sprintf(msg,"PCLK2  : %ld\r\n",HAL_RCC_GetPCLK2Freq());
    HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY);

	 while(1);

	 return 0;
}





void UART2_Init(void)
{
	huart2.Instance = USART2;
	huart2.Init.BaudRate=115200;
	huart2.Init.HwFlowCtl=UART_HWCONTROL_NONE ;
	huart2.Init.Mode=UART_MODE_TX_RX ;
//	huart2.Init.OverSampling=UART_OVERSAMPLING_16 ;
    huart2.Init.Parity=UART_PARITY_NONE ;
    huart2.Init.StopBits=UART_STOPBITS_1 ;
    huart2.Init.WordLength=UART_WORDLENGTH_8B ;

   if ( HAL_UART_Init(&huart2) != HAL_OK)
   {
	   //there is a problem
	   Error_handler();
   }
}

void Error_handler()
{
	while(1);
}

