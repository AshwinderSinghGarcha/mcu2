/*
 * msp.c
 *
 *  Created on: Dec 30, 2020
 *      Author: Developer
 */
#include "main.h"
#include "stm32f4xx_hal_gpio.h"
 void HAL_MspInit(void)
{
  /* NOTE : This function should not be modified, when the callback is needed,
            the HAL_MspInit could be implemented in the user file
   */
	 //1.priority grouping
	 // we use default one it is under hal driver layer under hal_cortex.c
	 HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);
	 //2.system exceptions
	 SCB->SHCSR |=SCB_SHCSR_USGFAULTENA_Msk ; //enable usage
	 SCB->SHCSR |=SCB_SHCSR_BUSFAULTENA_Msk ; //mem
	 SCB->SHCSR |=SCB_SHCSR_MEMFAULTENA_Msk ; //bus
	 //3.priority for system exceptions

	 HAL_NVIC_SetPriority(MemoryManagement_IRQn, 0, 0);
	 HAL_NVIC_SetPriority(BusFault_IRQn, 0, 0);
	 HAL_NVIC_SetPriority(UsageFault_IRQn, 0, 0);
}


  void HAL_TIM_IC_MspInit(TIM_HandleTypeDef *htim)
 {
	  //1.enable clk
	  __HAL_RCC_TIM2_CLK_ENABLE();
	  __HAL_RCC_GPIOA_CLK_ENABLE();

	  //2. configure gpio to behave as tim2 channel 1
	  GPIO_InitTypeDef TIM2_ch1;
	  TIM2_ch1.Pin      =GPIO_PIN_15 ;
	  TIM2_ch1.Mode     =GPIO_MODE_AF_PP;
	  TIM2_ch1.Alternate=GPIO_AF1_TIM2;
	  HAL_GPIO_Init(GPIOA,&TIM2_ch1);

	  //3.nvic settings
	  HAL_NVIC_SetPriority(TIM2_IRQn, 15, 0);
	  HAL_NVIC_EnableIRQ(TIM2_IRQn);
 }
