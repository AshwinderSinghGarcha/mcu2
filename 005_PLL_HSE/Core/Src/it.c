
/*
 * it.c
 *
 *  Created on: Dec 30, 2020
 *      Author: Developer
 */

#include "main.h"



void SysTick_Handler (void)
{
	HAL_IncTick();
	HAL_SYSTICK_IRQHandler();
}

