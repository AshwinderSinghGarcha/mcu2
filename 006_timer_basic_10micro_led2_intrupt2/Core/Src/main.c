
////-----------------------------pll to genearete 50mhz clock
/*
 * main.c
 *
 *  Created on: Dec 30, 2020
 *      Author: Developer
 */
#include<string.h>
#include<stdio.h>
#include "stm32f4xx_hal.h"
#include "main.h"

#define TRUE  1
#define FALSE 0


void SystemClock_Config();
void GPIO_inti();
void Error_handler();

void   TIMER6_Init();
TIM_HandleTypeDef htimer6;







int main (void)
{



    HAL_Init();
    SystemClock_Config();
    GPIO_inti();

    TIMER6_Init();
    while(1)
    {
    //lets start timer
    HAL_TIM_Base_Start_IT(&htimer6);

    }




return 0;
}


void SystemClock_Config()
{
 //we use by default clokc internal rc osc
	RCC_OscInitTypeDef osc_init;
	RCC_ClkInitTypeDef clk_init;

	osc_init.OscillatorType     =RCC_OSCILLATORTYPE_HSI;
	osc_init.HSIState           =RCC_HSI_ON;
	osc_init.HSICalibrationValue=RCC_HSICALIBRATION_DEFAULT;
	osc_init.PLL.PLLState       =RCC_PLL_ON;
	osc_init.PLL.PLLSource      =RCC_PLLSOURCE_HSI;
	osc_init.PLL.PLLM           =16;
	osc_init.PLL.PLLN           =100;
	osc_init.PLL.PLLP           =RCC_PLLP_DIV2;

	if (HAL_RCC_OscConfig(&osc_init)!= HAL_OK)
	{
		Error_handler();
	}

	clk_init.SYSCLKSource   = RCC_SYSCLKSOURCE_PLLCLK ;
	clk_init.ClockType =  RCC_CLOCKTYPE_SYSCLK |RCC_CLOCKTYPE_HCLK |\
                           RCC_CLOCKTYPE_PCLK1 |RCC_CLOCKTYPE_PCLK2;
	clk_init.AHBCLKDivider  = RCC_SYSCLK_DIV1;
    clk_init.APB1CLKDivider = RCC_HCLK_DIV2 ;
    clk_init.APB2CLKDivider = RCC_HCLK_DIV2 ;


	if (HAL_RCC_ClockConfig(&clk_init, FLASH_ACR_LATENCY_1WS)!= HAL_OK)
	{
		Error_handler();
	}

    /*--------------------------------configure systick
     *
     *
     */
    HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);              //4mhz/1000   hclk /1ms
    HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);
}


void GPIO_inti()
{
	__HAL_RCC_GPIOD_CLK_ENABLE();
	GPIO_InitTypeDef ledgpio;
	ledgpio.Mode =GPIO_MODE_OUTPUT_PP;
	ledgpio.Pin  =GPIO_PIN_12;
	ledgpio.Speed=GPIO_SPEED_FREQ_HIGH;
	ledgpio.Pull=GPIO_NOPULL;

	HAL_GPIO_Init(GPIOD, &ledgpio);
}

void Error_handler()
{
	while(1);
}

void TIMER6_Init()
{
	htimer6.Instance       = TIM6;
	htimer6.Init.Prescaler = 9;
	htimer6.Init.Period    = 50 -1;

	if (HAL_TIM_Base_Init(&htimer6)!=HAL_OK)
	{
		Error_handler();
	}
}




 void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	 HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_12);

}

