
////-----------------------------pll to genearete 50mhz clock
/*
 * main.c
 *
 *  Created on: Dec 30, 2020
 *      Author: Developer
 */
#include<string.h>
#include<stdio.h>
#include "stm32f4xx_hal.h"
#include "main.h"

#define TRUE  1
#define FALSE 0


void SystemClock_Config();
void GPIO_inti();
void Error_handler();

void   TIMER6_Init();
TIM_HandleTypeDef htimer6;







int main (void)
{



    HAL_Init();
    SystemClock_Config();
    GPIO_inti();

    TIMER6_Init();

    //lets start timer
    HAL_TIM_Base_Start(&htimer6);

    while(1)
    {
    	while (!(TIM6->SR & TIM_SR_UIF));
    	TIM6->SR =0;
    	HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_12);
    }



	 return 0;
}


void SystemClock_Config()
{
 //we use by default clokc internal rc osc
}


void GPIO_inti()
{
	__HAL_RCC_GPIOD_CLK_ENABLE();
	GPIO_InitTypeDef ledgpio;
	ledgpio.Mode =GPIO_MODE_OUTPUT_PP;
	ledgpio.Pin  =GPIO_PIN_12;
	ledgpio.Speed=GPIO_SPEED_FREQ_HIGH;
	ledgpio.Pull=GPIO_NOPULL;

	HAL_GPIO_Init(GPIOD, &ledgpio);
}

void Error_handler()
{
	while(1);
}

void TIMER6_Init()
{
	htimer6.Instance       = TIM6;
	htimer6.Init.Prescaler = 24;
	htimer6.Init.Period    = 64000 -1;

	if (HAL_TIM_Base_Init(&htimer6)!=HAL_OK)
	{
		Error_handler();
	}
}


void TIM6_DAC_IRQHandler ()
{
 //will use irq in next project
}
