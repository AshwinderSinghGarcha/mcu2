/*
 * msp.c
 *
 *  Created on: Dec 30, 2020
 *      Author: Developer
 */
#include "main.h"
#include "stm32f4xx_hal_gpio.h"
 void HAL_MspInit(void)
{
  /* NOTE : This function should not be modified, when the callback is needed,
            the HAL_MspInit could be implemented in the user file
   */
	 //1.priority grouping
	 // we use default one it is under hal driver layer under hal_cortex.c
	 HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);
	 //2.system exceptions
	 SCB->SHCSR |=SCB_SHCSR_USGFAULTENA_Msk ; //enable usage
	 SCB->SHCSR |=SCB_SHCSR_BUSFAULTENA_Msk ; //mem
	 SCB->SHCSR |=SCB_SHCSR_MEMFAULTENA_Msk ; //bus
	 //3.priority for system exceptions

	 HAL_NVIC_SetPriority(MemoryManagement_IRQn, 0, 0);
	 HAL_NVIC_SetPriority(BusFault_IRQn, 0, 0);
	 HAL_NVIC_SetPriority(UsageFault_IRQn, 0, 0);
}


 void HAL_TIM_Base_MspInit(TIM_HandleTypeDef *htimer)
 {
	 //1.enable clock for TIM6
	 __HAL_RCC_TIM6_CLK_ENABLE();

	 //2.enable IRQ TIM6
	 HAL_NVIC_EnableIRQ(TIM6_DAC_IRQn);

	 //3. setup priority for tim6
	 HAL_NVIC_SetPriority(TIM6_DAC_IRQn, 15, 0);


 }
